package oop.lab1;

/**
 * A simple model for a student with name and id.
 * 
 * @author Wanchanapon Thanwaranurak
 */
public class Student extends Person {
	/** the student's id must be the number character. */
	private long id;
	/**
	 * Initialize a new Student object.
	 * @param name is the name of the new Person
	 * @param id is the identification of the new student
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id; 
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if ( obj.getClass() != this.getClass() )
			return false;
		Student other = (Student)obj ;
		if ( this.id == other.id ) 
			return true;
		return false; 
	}
}
